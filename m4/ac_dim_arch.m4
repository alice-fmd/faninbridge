dnl -*- mode: autoconf -*-
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM_ARCH],
[
  AC_REQUIRE([AC_PROG_CC])
  dnl ------------------------------------------------------------------
  dnl Byte order 
  AH_TEMPLATE(MIPSEB, [Big-endian machine])
  AH_TEMPLATE(MIPSEL, [Little-endian machine])
  AC_C_BIGENDIAN([AC_DEFINE([MIPSEB])],[AC_DEFINE([MIPSEL])])
  AC_DEFINE([PROTOCOL],[1])

  AC_REQUIRE([AC_THREAD_FLAGS])
  dnl ------------------------------------------------------------------
  dnl Misc flags per host OS/CPU
  case $host_os:$host_cpu in
  sun*)		  AC_DEFINE([sunos])		;;
  solaris*)	  AC_DEFINE([solaris])	
		  LIBS="$LIBS -lsocket -lnsl"	;;
  hp-ux*)	  AC_DEFINE([hpux])		;;
  osf*)		  AC_DEFINE([osf])		;;
  aix*)		  AC_DEFINE([aix])	
		  AC_DEFINE([unix])
		  AC_DEFINE([_BSD])		;;
  lynxos*:rs6000) AC_DEFINE([LYNXOS])	
		  AC_DEFINE([RAID])
		  AC_DEFINE([unix])		
		  CPPFLAGS="$CPPFLAGS -I/usr/include/bsd -I/usr/include/posix"
		  LDFLAGS="$LDFLAGS -L/usr/posix/usr/lib"
		  LIBS="$LIBS -lbsd"		;;
  lynxos*:*86*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd -llynx"	;;
  lynxos*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd"		;;
  linux*)	  AC_DEFINE([linux])		
		  AC_DEFINE([unix])		;;
  esac
])

dnl
dnl EOF
dnl
