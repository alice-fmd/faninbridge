dnl -*- mode: autoconf -*-
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_THREAD_FLAGS],
[
  AC_PROVIDE([AC_THREAD_FLAGS])
  dnl ------------------------------------------------------------------
  dnl Thread flags to use 
  AC_MSG_CHECKING([for thread flags to use])
  type=
  case $host_os:$host_cpu in
  solaris*|sun*)	  type="-mt"
			  CFLAGS="$CFLAGS -mt"
			  CXXFLAGS="$CXXFLAGS -mt"
			  LIBS="$LIBS -lposix4"		;;
  hp-ux*|osf*|aix*)					;;
  # The DIM libary should not be threaded on an ARM chip, but the
  # FeeServer  assumes that it is so we take the next line out and
  # default to normal Linux
  # linux*:arm*)					;;
  linux*)		  type="-pthread"
			  CXXFLAGS="$CXXFLAGS -pthread"
			  LIBS="$LIBS -pthread"		;;
  lynxos*:rs6000)	  type="-mthrads"	
			  CFLAGS="$CFLAGS -mthreads"	
			  CXXFLAGS="$CXXFLAGS -mthreads";;
  *)			  LIBS="$LIBS -lpthread"	;;
  esac
  AC_MSG_RESULT([$type])
])
dnl
dnl EOF
dnl
