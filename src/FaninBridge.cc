#include <dim/dis.hxx>
#include <dim/dic.hxx>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sys/types.h>
#include <unistd.h>
#include <algorithm>
#include "Options.hh"

// === Service functions and classes =================================
namespace {
  struct to_lower
  {
    char operator()(char c)
    {
      return ::tolower(c);
    }
  };
  struct to_upper
  {
    char operator()(char c) 
    {
      return ::toupper(c);
    }
  };
  
  std::string& downcase(std::string& s)
  {
    std::transform(s.begin(), s.end(), s.begin(), to_lower());
    return s;
  }
  std::string& upcase(std::string& s)
  {
    std::transform(s.begin(), s.end(), s.begin(), to_upper());
    return s;
  }
  std::string concat(const std::string& s1, const std::string& s2, 
		      char sep='/') {
    std::stringstream s;
    s << s1;
    if (sep != '\0') s << sep;
    s << s2;
    return s.str();
  }
}

// === Our bridge class ==============================================
struct Bridge : public DimInfoHandler, public DimExitHandler
{
  /** 
   * in-bound (from LTU) Status data structure 
   */
  struct Status
  { 
    unsigned int mask; 
    unsigned int bits;
  };
  /**
   * out-bound (to LTU) Command data structure 
   */
  struct Command 
  {
    char det[16];
    unsigned int mask;
  };
  /**
   * In bound command 
   */
  struct InEnable : public DimCommand
  {
    InEnable(Bridge& bridge, const std::string& name)
      : DimCommand(name.c_str(), "I"), 
	itsBridge(bridge)
    {}
    void commandHandler()
    {
      int data = getInt();
      itsBridge.handleEnable(data);
    }
    Bridge& itsBridge;
  }; 
  /**
   * In bound command 
   */
  struct InSave : public DimCommand
  {
    InSave(Bridge& bridge, const std::string& name)
      : DimCommand(name.c_str(), "I"), 
	itsBridge(bridge)
    {}
    void commandHandler()
    {
      int data = getInt();
      itsBridge.handleSave(data);
    }
    Bridge& itsBridge;
  }; 


  Bridge(const std::string& udet, const std::string& ldet, 
	 unsigned short verbose)
    : itsOutMaskName(concat(udet, "BUSY_MASK")), 
      itsOutBitsName(concat(udet, "BUSY_BITS")), 
      itsInStatusName(concat(ldet, "STATUS")),
      itsInEnableName(concat(udet, "ENABLE_BUSY_BITS")), 
      itsInSaveName(concat(udet, "SAVE_BUSY_BITS")),
      itsOutMask(itsOutMaskName.c_str(), "I", 0), 
      itsOutBits(itsOutBitsName.c_str(), "I", 0), 
      itsInStatus(itsInStatusName.c_str(), 1, (void*)0, 0, this),
      itsInEnable(*this, itsInEnableName), 
      itsInSave(*this, itsInSaveName),
      itsEnable(), 
      itsSave(),
      itsStatus(),
      itsVerbose(verbose),
      itsInvalid(0xFFFFFFFF)
  {
    strcpy(itsEnable.det, ldet.c_str());
    strcpy(itsSave.det,   ldet.c_str());
    itsStatus.mask = 0xFFFFFFFF;
    itsStatus.bits = 0xFFFFFFFF;
    if (itsVerbose < 1) return;
    
    std::cout << "Subscribed to service\n" 
	      << "    " << itsInStatus.getName() << "\n" 
	      << "Sending commands on\n"
	      << "    ENABLE with " << itsEnable.det << "\n"
	      << "    SAVE   with " << itsSave.det << "\n"
	      << "Providing services\n"
	      << "    " << itsOutMask.getName() << "\n"
	      << "    " << itsOutBits.getName() << "\n" 
	      << "Providing commands\n" 
	      << "    " << itsInEnable.getName() << "\n"
	      << "    " << itsInSave.getName() << std::endl;
      
  }
  
  void handleCommand(const char* name, Command& cmd)
  {
    bool ret = DimClient::sendCommand(name, &cmd, sizeof(Command));
    if (itsVerbose < 1) return;
    if (ret) 
      std::cout << name << " command succeeded" << std::endl;
    else 
      std::cout << name << " command failed" << std::endl;
  }
    
  void handleEnable(int data)
  {
    itsEnable.mask = data;
    handleCommand("ENABLE", itsEnable);
  }
  void handleSave(int data)
  {
    itsSave.mask = data;
    handleCommand("SAVE", itsSave);
  }
  void infoHandler()
  {
    const int    need    = sizeof(Status);
    int          size    = itsInStatus.getSize();
    void*        data    = itsInStatus.getData();
    unsigned int oldMask = itsStatus.mask;
    unsigned int oldBits = itsStatus.bits;
    if (size < need || !data) { 
      itsStatus.mask = itsInvalid;
      itsStatus.bits = itsInvalid;
      std::cerr << "No link on status"
		<< " size=" << size 
		<< " data=" << data
		<< " need=" << need
		<< std::endl;
    }
    else {
      memcpy(&itsStatus, data, size);
      if (itsVerbose> 2) 
	std::cout << "Update of " << itsInStatus.getName() << ": "
		  << std::hex << std::setfill('0') 
		  << " mask=0x" << std::setw(8) << itsStatus.mask 
		  << " bits=0x" << std::setw(8) << itsStatus.bits 
		  << std::setfill(' ') << std::dec << std::endl;
    }
    if (oldMask == itsStatus.mask && 
	oldBits == itsStatus.bits) 
      // Nothing to update 
      return;
    updateServices();
  }
  void updateServices()
  {
    itsOutMask.setData(&itsStatus.mask, sizeof(unsigned int));
    itsOutBits.setData(&itsStatus.bits, sizeof(unsigned int));
    itsOutMask.updateService();
    itsOutMask.updateService();
    if (itsVerbose < 2)  return;
    std::cout << "Updated services " << itsOutMask.getName() << " and " 
	      << itsOutBits.getName() << std::endl;
  }
  void exitHandler(int code)
  {
    std::cerr << "FaninBridge exited with " << code << std::endl;
    switch (code) { 
    case DIMDNSUNDEF: std::cerr << "DNS: none defined";       break;
    case DIMDNSREFUS: std::cerr << "DNS: refused connection"; break;
    case DIMDNSDUPLC: std::cerr << "DNS: duplicate service";  break;
    case DIMDNSEXIT:  std::cerr << "DNS: died";               break;
    case DIMDNSTMOUT: std::cerr << "DNS: timeout";            break;
    case DIMDNSCNERR: std::cerr << "DNS: connection error";   break;
    case DIMDNSCNEST: std::cerr << "DNS: connected";          break;
    case DIMSVCDUPLC: std::cerr << "Service: duplicate";      break;
    case DIMSVCFORMT: std::cerr << "Service: bad format";     break;
    case DIMSVCINVAL: std::cerr << "Service: bad name";       break;
    case DIMTCPRDERR: std::cerr << "TCP: read error";         break;
    case DIMTCPWRRTY: std::cerr << "TCP: write error - retry";break;
    case DIMTCPWRTMO: std::cerr << "TCP: write time out";     break;
    case DIMTCPLNERR: std::cerr << "TCP: listen error";       break;
    case DIMTCPOPERR: std::cerr << "TCP: open server error";  break;
    case DIMTCPCNERR: std::cerr << "TCP: connection error";   break;
    case DIMTCPCNEST: std::cerr << "TCP: connected";          break;
    default:          std::cerr << "Unknown";                 break;
    }
    std::cerr << std::endl;
    itsStatus.mask = itsInvalid;
    itsStatus.bits = itsInvalid;
    updateServices();
  }

  // --- Names - saved to make sure data exists ----------------------
  std::string itsOutMaskName;
  std::string itsOutBitsName; 
  std::string itsInStatusName;
  std::string itsInEnableName;
  std::string itsInSaveName;
  // --- our out-bound services --------------------------------------
  DimService itsOutMask;
  DimService itsOutBits;
  // --- remote in-comming services ----------------------------------
  DimInfo    itsInStatus;
  // --- our incoming commands ---------------------------------------
  InEnable   itsInEnable;
  InSave     itsInSave;
  // --- Remote outbound command structures --------------------------
  Command    itsEnable;
  Command    itsSave;
  // --- Remote inbound data structures ------------------------------
  Status     itsStatus;
  unsigned short itsVerbose;
  const int  itsInvalid;;
};


// === The program ===================================================    
int
main(int argc, char** argv)
{
  Option<bool>         bOpt('b',"fork","Fork to background", false, false);
  Option<std::string>  pOpt('p',"pidfile", "PID file to write", "");
  Option<std::string>  fOpt('f',"from","Source DNS host", "aldaqecs");
  Option<std::string>  tOpt('t',"to","Destination DNS host","alifmddimdns");
  Option<std::string>  dOpt('d',"detector", "Detector name", "FMD");
  Option<bool>         hOpt('h',"help","Show usage informaiton", false, false);
  Option<bool>         vOpt('v',"verbose","Be verbose", false, false);
  CommandLine cl("");
  cl.Add(bOpt);
  cl.Add(pOpt);
  cl.Add(fOpt);
  cl.Add(tOpt);
  cl.Add(dOpt);
  cl.Add(hOpt);
  cl.Add(vOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }    

  bool           tobg     = bOpt;
  std::string    pidout   = pOpt;
  std::string    toHost   = tOpt;
  std::string    fromHost = fOpt;
  std::string    detName  = dOpt;
  unsigned short verbose  = vOpt.Count();

  std::cout << "Running " << PACKAGE_NAME << "\n"
	    << std::boolalpha 
	    << "  Fork:       " << tobg << "\n"
	    << "  PID file:   " << pidout << "\n"
	    << "  To host:    " << toHost << "\n"
	    << "  From host:  " << fromHost << "\n"
	    << "  Detector:   " << detName << "\n"
	    << "  Verbose:    " << verbose << "\n"
	    << std::noboolalpha << std::endl;

 
  if (tobg) { 
    pid_t pid = fork();
    if (pid == -1) { 
      std::cerr << argv[0] << ": failed to fork" << std::endl;
      return 1;
    }
    if (pid != 0) { // parent 
      return 0;
    }
  }

  if (!pidout.empty()) { 
    std::ofstream pidf(pidout.c_str());
    if (!pidf) { 
      std::cerr << argv[0] << ": Failed to make PID file " 
		<< pidout << std::endl;
      return 1;
    }
    pidf << getpid() << std::endl;
    pidf.close();
  }

  DimClient::setDnsNode(fromHost.c_str());
  DimServer::setDnsNode(toHost.c_str());

  std::string ldet(detName);
  std::string udet(detName);
  downcase(ldet);
  upcase(udet);
  Bridge bridge(udet, ldet, verbose);

  std::string servName(concat(udet,"FANIN", '_'));
  DimServer::start(servName.c_str());
  bridge.infoHandler();
  
  while (true) sleep(1);

  return 0;
}
